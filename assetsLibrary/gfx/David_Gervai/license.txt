All tiles have been drawn by David E. Gervais, and are published under the Creative Commons licence.
You are free to to copy, distribute and transmit those tiles as long as you credit David Gervais as their creator.
Source : http://pousse.rapiere.free.fr/tome/
License : http://creativecommons.org/licenses/by/3.0/