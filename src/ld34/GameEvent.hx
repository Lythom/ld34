package ld34;
import ld34.ActionResolver.ActionResult;
import ld34.Room.RoomContent;
import openfl.events.Event;

enum GameEventEnum {
	actionStart(content:RoomContent);
	actionResolved(result:ActionResult);
	gotoNextRoomTransition(room:Room);
	arrivedToNewRoom(room:Room);
}

/**
 * ...
 * @author Lythom
 */
class GameEvent extends Event
{
	public var nextRoom:Room;
	public static var GAME_EVENT_HAPPENED = "GAME_EVENT_HAPPENED";
	public var gameEvent:GameEventEnum;

	public function new(gameEvent:GameEventEnum, nextRoom:Room) 
	{
		super(GAME_EVENT_HAPPENED, bubbles, cancelable);
		this.nextRoom = nextRoom;
		this.gameEvent = gameEvent;
		
	}
	
}