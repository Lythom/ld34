package ld34.crew;
import com.haxepunk.graphics.Image;
import haxe.ds.WeakMap;
import ld34.crew.CrewMember;
import com.haxepunk.gui.Control;
import motion.Actuate;
import motion.easing.Linear;
import openfl.Assets;
import openfl.display.BitmapData;
import tools.attributes.Bonus;
import tools.attributes.Item;
import tools.image.Scale3XUpscaler;

/**
 * ...
 * @author Lythom
 */
class CrewMember extends Item
{
	var levelItem:Item;

	public var image:Control = new Control();
	public var imageIcon(get, null):CrewMemberIcon;
	public var lore(get, null):String;
	public var value(get, null):Int;
	public var level:Float = 1;
	public var strength:String;

	static public inline var NORMAL:String = "normal";
	static public inline var ELITE:String = "elite";
	public var type:String = null;
	static private var imgToCrew:WeakMap<Control, CrewMember> = new WeakMap<Control, CrewMember>();
	var iconlocalYbackup:Float = -1;
	var iconlocalXbackup:Float = -1;

	public function new(name:String)
	{
		super(name);
	}

	override public function add(bonus:Bonus):Bool
	{
		if (this.type != null) throw 'Only one typed bonus can be added on a crew member';
		var result = super.add(bonus);

		this.type = this._baseAttributes.keys().next();
		
		var contentIconPath = 'gfx/crew/$type.png';
		var contentIcon =  Assets.getBitmapData(contentIconPath);
		addImage(contentIcon);

		return result;
	}

	public function addImage(bmpData:BitmapData):Void {
		var img = new Image(bmpData);
		if (strength == ELITE) {
			img.tintMode = Image.TINTING_MULTIPLY;
			img.tinting = 0.7;
			img.color = 0xE24D1B;
		}
		
		this.image.graphic = img;
	}

	public function die() {
		cast(imageIcon.graphic, Image).angle = 0;
		Actuate.tween(cast(this.imageIcon.graphic, Image), 1.5, {
			angle: -360
		});
		Actuate.tween(this.imageIcon, 1.5, {
			alpha: 0,
			localY: -50,
		}).onComplete(function() {
			if (this.imageIcon != null && this.imageIcon.container != null) {
				this.imageIcon.container.removeControl(this.imageIcon);
			}
		});

		imgToCrew.remove(this.imageIcon);
	}
	
	public function animateAsAttacker() 
	{
		if (iconlocalYbackup == -1 && iconlocalXbackup == -1) {
			iconlocalXbackup = imageIcon.localX;
			iconlocalYbackup = imageIcon.localY ;
			Actuate.tween(imageIcon, 0.4, { localY: -30 }, true).reflect().repeat();
		}
	}
	
	public function animateWillLevelUp(toLevel:Int) 
	{
		if (toLevel > this.level) {
			var levelDiff = toLevel - this.level;
			imageIcon.levelUpIndicator.show();
			imageIcon.levelUpIndicator.text = "+" + levelDiff;
		}
	}
	
	public function animateAsDefender() 
	{
		if (iconlocalYbackup == -1 && iconlocalXbackup == -1) {
			iconlocalXbackup = imageIcon.localX;
			iconlocalYbackup = imageIcon.localY ;
			Actuate.tween(imageIcon, 0.1, { localX: imageIcon.localX + 3 }, true).ease(Linear.easeNone).reflect().repeat();
		}
	}
	
	public function animationStop() 
	{
		if (iconlocalYbackup != -1 && iconlocalXbackup != -1) {
			Actuate.stop(imageIcon);
			imageIcon.localY = iconlocalYbackup;
			iconlocalYbackup = -1;
			imageIcon.localX = iconlocalXbackup;
			iconlocalXbackup = -1;
		}
		imageIcon.levelUpIndicator.hide();
	}

	function get_value():Int
	{
		this.refresh();
		var attrValue:Float = this.get(type).value; // value obtained from levels
		return Math.round(attrValue);
	}

	function get_lore():String
	{
		var bonus = this.bonusList.first();
		if (bonus != null) {
			return bonus.label;
 		} else {
			return "";
		}
	}

	override public function toString():String
	{
		return '$name ($type Lv.$level) - $lore';
	}

	public function setLevel(lvl:Float)
	{
		this.level = lvl;

		if (levelItem != null) {
			this.unequip(levelItem);
		}
		levelItem = new Item("Level");

		var levelMult = (Math.pow(lvl, Config.levelPow) * (Config.levelRatio));
		levelItem.addMultiplicationBonus(this.type, Std.int(levelMult), 0);
		this.equip(levelItem);

	}

	function get_imageIcon():CrewMemberIcon
	{
		if (imageIcon == null) {
			imageIcon = new CrewMemberIcon(this);
			imgToCrew.set(imageIcon, this);
		}
		return imageIcon;
	}

	public static function getCrewMemberFromImageIcon(imageIcon:Control):CrewMember {
		return imgToCrew.get(imageIcon);
	}
}