package ld34.crew;

import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.FormatAlign;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import flash.events.Event;
import openfl.Assets;

/**
 * ...
 * @author Lythom
 */
class CrewMemberIcon extends Control
{
	public var levelUpIndicator:Button;
	var crewMember:CrewMember;
	var info:com.haxepunk.gui.Panel;
	var infolabel:com.haxepunk.gui.Label;

	public function new(crewMember:CrewMember) 
	{
		this.crewMember = crewMember;
		var type = crewMember.type;
		var bmpD = Assets.getBitmapData('gfx/crewicon/$type.png');
		
		super(x, y, bmpD.width, bmpD.height);
		
		var img = new Image(bmpD);
		
		if (crewMember.strength == CrewMember.ELITE) {
			img.tintMode = Image.TINTING_MULTIPLY;
			img.tinting = 0.8;
			img.color = 0xE24D1B;
		}
		
		this.graphic = img;
		
		GameState.guiUseSkin(type);
		info = new Panel(0, -120, 300, 100);
		infolabel = new Label("", 12, 10, 0, 0, FormatAlign.CENTER, VerticalAlign.CENTER);
		infolabel.color = 0x142717;
		info.addControl(infolabel);
		this.addControl(info);
		info.hide();
		
		this.levelUpIndicator = new Button("+100", 0, -10, 30, 30);
		this.levelUpIndicator.enabled = false;
		this.levelUpIndicator.color = 0x143C13;
		this.levelUpIndicator.size = 16;
		this.levelUpIndicator.padding = -4;
		addControl(this.levelUpIndicator);
		this.levelUpIndicator.hide();
		
		this.addEventListener(Control.MOUSE_HOVER, displayInfo);
		this.addEventListener(Control.MOUSE_OUT, hideInfo);
		
	}
	
	public function updateLabelInfo() {
		infolabel.text = '${crewMember.type} Lvl.${crewMember.level}\nPower : ${crewMember.value}\nName : ${crewMember.name}';
	}
	
	private function hideInfo(e:Event):Void 
	{
		info.hide();
	}
	
	private function displayInfo(e:Event):Void 
	{
		updateLabelInfo();
		info.show();
	}
	
}