package ld34;
import ld34.components.RoomControl;
import ld34.crew.CrewMember;

enum RoomContent
{
	MonsterFight(m:Monster);
	CrewMember(c:CrewMember);
}

/**
 * ...
 * @author Lythom
 */
class Room
{

	public var roomL:Room = null;
	public var roomR:Room = null;
	public var contenu:RoomContent = null;
	public var stage:Int = 1;
	public var control:RoomControl;

	public function new(roomL:Room = null, roomR:Room = null)
	{
        this.roomL = roomL;
        this.roomR = roomR;
    }

}