package ld34 ;
import ld34.crew.Crew;
import ld34.crew.CrewMember;
import ld34.crew.CrewMember;
import ld34.Room;
import tools.attributes.Item;

class GeneratorWorld
{
	public function new()
	{

	}

	public static function createDungeon():Room
	{
		var roomListFirstLv:Array<Room> = new Array<Room>();

		for (i in 0...Config.numberOfLevel)
		{
			roomListFirstLv.push(new Room());
		}
		var roomListComplet = generateParentsRoom(roomListFirstLv);

		return roomListComplet[roomListComplet.length-1];
	}

	private static function generateParentsRoom(roomList:Array<Room>):Array<Room>
	{
		var roomListParents:Array<Room> = new Array<Room>();

		for (i in 0...roomList.length - 1)
		{
			roomListParents.push(new Room(roomList[i],roomList[i+1]));
		}

		if (roomListParents.length == 1)
		{
			return roomListParents;
		} else
		{
			return generateParentsRoom(roomListParents);
		}
	}

	public static function GenerateContenuRoom(room:Room)
	{
		if (room != null)
		{
			createFirstRoom(room, GameState.roomCount, GameState.stageDeepInc);
		}
	}

	private static function createFirstRoom(room:Room, niveau:Int, stageDeepInc:Int, parentRoom:Room = null)
	{
		if (room.contenu == null)
		{
			if (parentRoom != null)
			{
				room.stage = findDeepRoom(parentRoom.stage, stageDeepInc);
				if (parentRoom.stage != room.stage)
				{
					stageDeepInc = 0;
				}
			}

			if (niveau == Config.numberOfLevel)
			{
				var monster:Monster = Monster.generateFinalBossMonster(room.stage, niveau);
				room.contenu = RoomContent.MonsterFight(monster);
			}
			else if (Random.int(0, 100) <= Config.generateMonsterProportion)
			{
				var monster:Monster = Monster.generateMonster(room.stage, niveau);
				room.contenu = RoomContent.MonsterFight(monster);
			}
			else
			{
				var crew:CrewMember = Crew.generateCrewMember(room.stage);
				room.contenu = RoomContent.CrewMember(crew);
			}
		}

		if (room.roomL != null && room.roomR != null && niveau - GameState.roomCount <= Config.deepArmyView)
		{
			var newNiveau:Int = niveau + 1;
			var newStageDeepInc:Int = stageDeepInc + 1;
			createFirstRoom(room.roomL, newNiveau, newStageDeepInc, room);
			createFirstRoom(room.roomR, newNiveau, newStageDeepInc, room);
		}
	}

	private static function randomElementMonster():String
	{
		var result:String = "";

		switch(Random.int(1, 3))
		{
			case 1:
				result = GameState.PAPER;
			case 2:
				result = GameState.CISOR;
			case 3 :
				result = GameState.ROCK;
		}
		return result;
	}

	private static function findDeepRoom(stage:Int, stageDeepInc:Int):Int
	{
		var result:Int = stage;

		// mettre ici le petit nalgo qui va bien
		if (stageDeepInc >= Config.stageDeepIncMin)
		{
			if (Random.int(Config.stageDeepIncMin, Config.stageDeepIncMax) <= stageDeepInc)
			{
				result++;
			}
		}

		return result;
	}
}