package ld34;
import com.haxepunk.gui.Control;
import ld34.crew.Crew;
import tools.attributes.AttributeSet;

/**
 * ...
 * @author Lythom
 */
class GameState
{
	static public inline var PAPER:String = "paper";
	static public inline var ROCK:String = "rock";
	static public inline var CISOR:String = "scissors";

	public static var room:Room = new Room();
	public static var armee:AttributeSet;
	static public var roomCount:Int = 1;
	static public var stageDeepInc:Int = 4;

	public static function strongAgainst(attr:String):String {
		switch(attr) {
			case PAPER:
				return CISOR;
			case ROCK:
				return PAPER;
			case CISOR:
				return ROCK;
			default:
				return null;
		}
	}

	public static function weakAgainst(attr:String):String {
		switch(attr) {
			case PAPER:
				return ROCK;
			case ROCK:
				return CISOR;
			case CISOR:
				return PAPER;
			default:
				return null;
		}
	}
	
	public static function guiUseSkin(attr:String = "") {
		switch(attr) {
			case PAPER:
				Control.useSkin('gfx/ui/paper.png');
			case ROCK:
				Control.useSkin('gfx/ui/rock.png');
			case CISOR:
				Control.useSkin('gfx/ui/scissor.png');
			case "transparent":
				Control.useSkin('gfx/ui/transparent.png');
			default:
				 Control.useSkin('gfx/ui/magdaBlue.png');
		}
	}

	public static function newGame() {


		GameState.roomCount = 1;
		GameState.stageDeepInc = 0;
		GameState.armee = new AttributeSet();

		GameState.armee.get(PAPER).baseValue = 0;
		GameState.armee.get(ROCK).baseValue = 0;
		GameState.armee.get(CISOR).baseValue = 0;

		var initialMembers = Crew.generateInitialMembers(3);
		initialMembers.push(Crew.generateCrewMember(1));
		for (m in initialMembers) {
			GameState.armee.equip(m);
		}

		GameState.room = GeneratorWorld.createDungeon();
		GeneratorWorld.GenerateContenuRoom(GameState.room);

	}



}