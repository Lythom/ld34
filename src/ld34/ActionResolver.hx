package ld34;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import ld34.crew.CrewMember;
import ld34.GameEvent.GameEventEnum;
import ld34.Room.RoomContent;
import motion.Actuate;
import openfl.events.EventDispatcher;
import tools.attributes.AttributeSet;
import tools.attributes.Item;

using Lambda;

enum ActionResult {
	MonsterSuccess(deadCrew:Array<CrewMember>, monster:Monster);
	GameOver(deadCrew:Array<CrewMember>);
	Recruitment(recruited:Array<CrewMember>);
	GameWon;
}

/**
 * ...
 * @author Lythom
 */
class ActionResolver extends EventDispatcher
{
	
	public static function getInstance():ActionResolver 
	{
		if (_instance == null) {
			_instance = new ActionResolver();
			dumpStats();
		}
		return _instance;
	}
	private static var _instance:ActionResolver = null;
	
	public var locked:Bool = false;
	
	public function resolveAction(nextRoom:Room):Void 
	{
		if (locked) return;
		locked = true;
		
		var content = nextRoom.contenu;
		dispatchEvent(new GameEvent(GameEventEnum.actionStart(content), nextRoom));
		
		var result:ActionResult = null;
		switch (content) 
		{
			case RoomContent.MonsterFight(monster):
				trace('Fighting ' + Std.string(monster));
				result = fightAMonster(monster);
				
			case RoomContent.CrewMember(crewMember):
				result = recruitAMember(crewMember);
		}

		Actuate.timer(Config.actionStartingAnimDurationInSec).onComplete(function() {
			switch (result) 
			{
				case ActionResult.MonsterSuccess(deadCrew, monster):
					trace('Victory ! Are dead : ' + deadCrew.map(function(c:CrewMember) { return c.toString() + ','; } ));
					// TODO : visuall representation of the action
					
				case ActionResult.GameOver:
					trace('Game Over');
					locked = false;
					// TODO : visuall representation of the action
					
				case ActionResult.Recruitment(recruited):
					trace('recruited : ' + recruited.map(function(c:CrewMember) { return c.toString() + ','; } ));
					// TODO : visuall representation of the action
					
				case ActionResult.GameWon:
					trace('Victory');
					locked = false;
					// TODO : visuall representation of the action
			}
			dispatchEvent(new GameEvent(GameEventEnum.actionResolved(result), nextRoom));
			
			if (Type.enumIndex(result) != Type.enumIndex(ActionResult.GameOver(null)) && !Type.enumEq(result, ActionResult.GameWon)) {

				Actuate.timer(Config.actionResolvedAnimDurationInSec).onComplete(function() {
					trace("moving to a new room");
					dispatchEvent(new GameEvent(GameEventEnum.gotoNextRoomTransition(nextRoom), nextRoom));
					
					Actuate.timer(Config.nextTransitionAnimDurationInSec).onComplete(function() {
						trace("Choose wisely !");
						
						GameState.roomCount ++;
						if (GameState.room.stage != nextRoom.stage) {
							GameState.stageDeepInc = 0;
						} else {
							GameState.stageDeepInc++;
						}
						GameState.room = nextRoom;
						dispatchEvent(new GameEvent(GameEventEnum.arrivedToNewRoom(nextRoom), nextRoom));
						
						locked = false;
					});
				});

			}
		});
		
		dumpStats();
		
	}

	function recruitAMember(crewMember:CrewMember):ActionResult
	{
		GameState.armee.equip(crewMember);
		return ActionResult.Recruitment([crewMember]);
	}

	function fightAMonster(monster:Monster):ActionResult
	{
		
		var defenseAttr:String = GameState.weakAgainst(monster.attr);
		var attackAttr = GameState.strongAgainst(monster.attr);
		var crewMembers:List<CrewMember> = GameState.armee.items.map(function itemToCrewMember(item:Item):CrewMember { return cast(item, CrewMember); } );
		
		function crewMemberIsWeakType(crewMember:CrewMember):Bool
		{
			return crewMember.get(defenseAttr).value > 0;
		}
		
		var deadCrew:Array<CrewMember> = new Array<CrewMember>();
		
		if (GameState.armee.get(attackAttr).value == 0) {
			trace('no enough attack !');
			return ActionResult.GameOver(deadCrew);
		}
		
		while ( monster.value > 0 && crewMembers.exists(crewMemberIsWeakType)) {
			
			trace('Fight round start');
			
			// Deal monter damages on the crew
			var attackDamage:Int = monster.value;
			var weakTypeCrew:List<CrewMember> = crewMembers.filter(crewMemberIsWeakType);
			
			while (attackDamage > 0 && weakTypeCrew.length > 0) {
				trace('Monster attacking with $attackDamage');
				var defender = Random.fromIterable(weakTypeCrew);
				trace('defender : '+defender.toString());
				var defenderResistance:Float = defender.value; // value obtained from levels
				
				attackDamage -= Math.round(defenderResistance);
				trace('defender reduces attackDamage by $defenderResistance, remaing $attackDamage');
				
				// ups, defender was killed !
				if (attackDamage >= 0) {
					trace('defender was killed because $defenderResistance was < to initial attackDamage');
					weakTypeCrew.remove(defender);
					crewMembers.remove(defender);
					GameState.armee.unequip(defender);
					deadCrew.push(defender);
				}
			}
			
			// Deal crew damages on the Monster
			var crewAttack:Int = Math.round(GameState.armee.get(attackAttr).value);
			monster.value -= crewAttack;
			trace('defenders reduces monster value by $crewAttack, remaing ${monster.value}');
		}
		
		if (crewMembers.exists(crewMemberIsWeakType)) {
			crewMembers.iter(function(c) {
				if (c.level < monster.level) {
					c.setLevel(monster.level);
				}
			});
			if (monster.strength == Monster.FINALBOSS) {
				return ActionResult.GameWon;
			} else {
				return ActionResult.MonsterSuccess(deadCrew, monster);
			}
		} else {
			return ActionResult.GameOver(deadCrew);
		}
	}

	
	public static function dumpStats() {
		
		var str = "Attributes Values :\n";
		for (s in GameState.armee.baseAttributes) {
			str += s.toString();
		}
		var crewMembers:List<CrewMember> = GameState.armee.items.map(function itemToCrewMember(item:Item):CrewMember { return cast(item, CrewMember); } );
		for (c in crewMembers) {
			str += (c) + "\n";
		}
		trace(str);
	}

}