package ld34;

/* ...
 * @author LesLemmings
 */
class Config
{
	public static var numberOfLevel:Int = 30;

	public static var deepArmyView:Int = 5;

	public static var stageDeepIncMin:Int = 3;
	public static var stageDeepIncMax:Int = 7;

	public static var powerCrewMin:Int = 1;
	public static var powerCrewMax:Int = 2;

	public static var powerCrewEliteMin:Int = 4;
	public static var powerCrewEliteMax:Int = 5;

	public static var powerNormalMonsterMin:Int = 2;
	public static var powerNormalMonsterMax:Int = 3;

	public static var powerEliteMonsterMin:Int = 4;
	public static var powerEliteMonsterMax:Int = 5;

	public static var powerBossMonsterMin:Int = 7;
	public static var powerBossMonsterMax:Int = 8;

	//public static var coefPowerMonsterbyLevel:Float = 1;
	public static var coefPowerMonsterbyLevel:Float = 0.4;
	public static var powPowerMonsterbyLevel:Float = 1.3;

	//in %
	public static var generateLowTypeCrew:Int = 50;
	public static var generateEliteCrew:Int = 12;
	public static var generateBossMonster:Int = 3;
	public static var generateEliteMonster:Int = 15;
	public static var generateMonsterProportion:Int = 60;

	public static var actionStartingAnimDurationInSec:Int = 0;
	public static var actionResolvedAnimDurationInSec:Int = 1;
	public static var nextTransitionAnimDurationInSec:Int = 1;

	//public static var levelRatio:Float = 0.75;
	public static var levelRatio:Float = 0.4;
	public static var levelPow:Float = 1.3;
}