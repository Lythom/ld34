package ld34.components;
import com.haxepunk.graphics.Image;
import ld34.components.RoomControl;
import com.haxepunk.gui.Control;
import flash.display.Shape;
import ld34.Room;
import motion.Actuate;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.geom.Matrix;

using Lambda;

/**
 * ...
 * @author Lythom
 */
class LevelGraph extends Control
{
	var room:Room = null;
	var levels:Array<Array<Control>> = null;
	var path:BitmapData;
	var youAreHere:Control;
	
	public function new(x:Float=0, y:Float=0, width:Int=0, height:Int=0) 
	{
		super(x, y, width, height);
		
		this.path = new BitmapData(width, height, true, 0x00FFFFFF);
		graphic = new Image(path);
		
		// circle here
		var circleShape:Shape = new Shape();
		circleShape.graphics.lineStyle(4, 0xF50707);
		circleShape.graphics.drawCircle(30, 30, 25);
		var circle:BitmapData = new BitmapData(Math.ceil(circleShape.width+6), Math.ceil(circleShape.height+6), true, 0x00000000);
		circle.draw(circleShape, null, null, null, null, true);
		
		this.youAreHere = new Control();
		this.youAreHere.graphic = new Image(circle);
		this.youAreHere.graphic.x = - circle.width/2;
		this.youAreHere.graphic.y = - circle.height/2;
		this.youAreHere.localX = getHereX();
		this.youAreHere.localY = getHereY();
	}
	
	
	public function display(room:Room, maxDepth:Int) 
	{
		this.removeAllControls();
		
		addControl(this.youAreHere);
		
		// init
		levels = new Array<Array<Control>>();
		for (i in 0...maxDepth) {
			levels[i] = new Array<Control>();
		}
		
		// recursive update
		displayR(room.roomL, maxDepth, 1);
		displayR(room.roomR, maxDepth, 1);
		
		// final format
		for (roomList in levels) {
			var i = 0;
			for (roomCtrl in roomList) {
				roomCtrl.localX = getXForLevel(i, roomList.length);
				i++;
			}
		}
		
		// path
		path.fillRect(path.rect, 0x00FFFFFF);
		var shape:Shape = new Shape();
		shape.graphics.lineStyle(5, 0x2F3440);
		var leftOffset:Int = 0;
		var topOffset:Int = 0;
		for (level in 0...levels.length - 1) {
			for (roomId in 0...levels[level].length) {
				if (levels[level + 1].length > 0) {
					shape.graphics.moveTo(getXForLevel(roomId, levels[level].length), getYFromDepth(level, maxDepth));
					shape.graphics.lineTo(getXForLevel(roomId, levels[level+1].length), getYFromDepth(level + 1, maxDepth));
					shape.graphics.moveTo(getXForLevel(roomId, levels[level].length), getYFromDepth(level, maxDepth));
					shape.graphics.lineTo(getXForLevel(roomId + 1, levels[level + 1].length), getYFromDepth(level + 1, maxDepth));
				}
			}
		}
		// from here
		shape.graphics.moveTo(getXForLevel(0, 1), getYFromDepth(0, maxDepth));
		shape.graphics.lineTo(getXForLevel(0, 2), getYFromDepth(1, maxDepth));
		shape.graphics.moveTo(getXForLevel(0, 1), getYFromDepth(0, maxDepth));
		shape.graphics.lineTo(getXForLevel(1, 2), getYFromDepth(1, maxDepth));
		
		var m:Matrix = new Matrix();
		m.tx = leftOffset;
		m.ty = topOffset;
		path.draw(shape, m);
		graphic = new Image(path);
		
		
	}
	
	private function displayR(room:Room, maxDepth:Int, depth:Int) 
	{
		if (depth < maxDepth && room != null) {	
			if (room.control == null) {
				room.control = new RoomControl(room);
			}
			if (room.control.container != this) {
				addControl(room.control);
			}
			room.control.localY = getYFromDepth(depth, maxDepth);
			if (!levels[depth].has(room.control)) {
				levels[depth].push(room.control);
			}
			
			displayR(room.roomL, maxDepth, depth + 1);
			displayR(room.roomR, maxDepth, depth + 1);
		}
	}
	
	public function getXForLevel(index:Int, Count:Int):Int
	{
		var ampitude:Int = Math.round((this.width) / (Count + 1));
		return Math.round(ampitude * (index + 1));
	}
	
	public function getYFromDepth(depth:Int, maxDepth:Int):Int
	{
		var ampitude:Int = Math.round((this.height) / (maxDepth));
		var calclocaly:Int = this.height - ampitude * depth;
		return calclocaly - 40;
	}
	
	private function getHereX():Int {
		return Math.round(this.width / 2 - this.youAreHere.width / 2);
	}
	
	private function getHereY():Int {
		return getYFromDepth(0, Config.deepArmyView);
	}
	
	public function previewLeft() {
		Actuate.tween(this.youAreHere, 0.5, {
			localX: getXForLevel(0, 2),
			localY: getYFromDepth(1, Config.deepArmyView)
		});
	}
	
	public function previewRight() {
		Actuate.tween(this.youAreHere, 0.5, {
			localX: getXForLevel(1, 2),
			localY: getYFromDepth(1, Config.deepArmyView)
		});
	}
	
	public function resetPreview() {
		Actuate.tween(this.youAreHere, 0, {
			localX: getHereX(),
			localY: getHereY()
		});
	}
	
}