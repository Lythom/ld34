package ld34.components;
import com.haxepunk.graphics.Graphiclist;
import ld34.components.RoomControl;
import ld34.crew.CrewMember;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Control;
import openfl.display.BitmapData;
import ld34.Room;
import openfl.Assets;

/**
 * ...
 * @author Lythom
 */
class RoomControl extends Control
{

	public function new(room:Room, x:Float=0, y:Float=0, width:Int=0, height:Int=0)
	{
		super(x, y, width, height);
		var type = "no";
		var contentIconPath = "";
		var contentIcon:Image;
		var bgroomPath:String = "";
		GameState.room;
		switch (room.contenu)
		{
			case RoomContent.MonsterFight(monster):
				type = monster.attr;
				bgroomPath = 'gfx/roombgicon/monster.png';
				contentIconPath = 'gfx/monstericon/$type.png';
				contentIcon = new Image(Assets.getBitmapData(contentIconPath));
				contentIcon.tinting = 0.8;
				contentIcon.tintMode = Image.TINTING_MULTIPLY;
				switch(monster.strength) {
					case "boss":
						contentIcon.color = 0xAA44AC;
					case "elite":
						contentIcon.color = 0xCEA822;
				}

			case RoomContent.CrewMember(crewMember):
				type = crewMember.type;
				bgroomPath = 'gfx/roombgicon/crew.png';
				contentIconPath = 'gfx/crewicon/$type.png';
				contentIcon = new Image(Assets.getBitmapData(contentIconPath));
				contentIcon.tinting = 0.8;
				contentIcon.tintMode = Image.TINTING_MULTIPLY;
				if (crewMember.strength == "elite") {
					contentIcon.color = 0xE24D1B;
				}
		}

		var bgroom = Assets.getBitmapData(bgroomPath);
		var image:Image = new Image(bgroom);
		//var paths:Image = new Image();
		var list:Graphiclist = new Graphiclist([image, contentIcon]);
		list.x = -bgroom.width / 2;
		list.y = -bgroom.height / 2;
		graphic = list;
	}

}
