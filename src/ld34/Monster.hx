package ld34;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Control;
import ld34.crew.Crew;
import openfl.Assets;
import openfl.display.BitmapData;
import tools.image.Scale3XUpscaler;

/**
 * ...
 * @author Lythom
 */
class Monster
{
	public var attr:String;
	public var value:Int;
	public var name:String;
	public var image:Control;
	public var strength:String;
	public var level:Int;

	static public inline var NORMAL:String = "normal";
	static public inline var ELITE:String = "elite";
	static public inline var BOSS:String = "boss";
	static public inline var FINALBOSS:String = "boss";

	public function new(attr:String = "", value:Int = 0, name:String = "Michel", strength:String = "normal")
	{
		this.value = value;
		this.attr = attr;
		this.name = name;
	}

	public static function generateMonster(stage:Int, level:Int):Monster
	{
		var result:Monster = new Monster();

		result.strength = findStrengthMonster();
		result.attr = findElementMonster();
		result.level = findLevelMonster(result.strength, stage);
		result.value = Std.int(findPowerMonster(result.strength, result.level, level));
		result.name = Crew.generateMonsterName();
		result.image = findImageMonster(result.strength, result.attr);

		return result;
	}

	public static function generateFinalBossMonster(stage:Int, level:Int):Monster
	{
		var result:Monster = new Monster();

		result.strength = Monster.FINALBOSS;
		result.attr = findElementMonster();
		result.level = findLevelMonster(result.strength, stage);
		result.value = Std.int(findPowerMonster(result.strength, result.level, level));
		result.name =  Crew.generateMonsterName();
		result.image = findImageMonster(result.strength, result.attr);

		return result;
	}

	private static function findLevelMonster(strength:String, stage:Int):Int
	{
		var result:Int = stage + 1;

		if (strength == Monster.BOSS)
		{
			result = stage + 3;
		}
		else if (strength == Monster.ELITE)
		{
			result = stage + 2;
		}

		return result;

	}

	private static function findStrengthMonster():String
	{
		var strength:String = "";

		if (Random.float(0, 100) <= ((Config.generateBossMonster * dungeonProgress()) / 100))
		{
			strength = Monster.BOSS;
		}
		else if (Random.float(0, 100) <= ((Config.generateEliteMonster * dungeonProgress()) / 100))
		{
			strength = Monster.ELITE;
		}
		else
		{
			strength = Monster.NORMAL;
		}

		return strength;
	}

	private static function dungeonProgress():Float
	{
		return (GameState.roomCount * 100) / Config.numberOfLevel;
	}

	private static function findElementMonster()
	{
		var choses:Int = Random.int(1, 3);
		if (choses == 1)
		{
			return GameState.PAPER;
		}
		else if (choses == 2)
		{
			return GameState.ROCK;
		}
		else if (choses == 3)
		{
			return GameState.CISOR;
		}
		else
		{
			return GameState.PAPER;
		}
	}

	private static function findPowerMonster(strength:String, levelMonster:Int, level:Int):Float
	{
		var valeurBase:Float = 1;

		if (strength == Monster.BOSS)
		{
			valeurBase = Random.int(Config.powerBossMonsterMin, Config.powerBossMonsterMax);
		}
		else if (strength == Monster.ELITE)
		{
			valeurBase = Random.int(Config.powerEliteMonsterMin, Config.powerEliteMonsterMax);
		}
		else if (strength == Monster.NORMAL)
		{
			valeurBase = Random.int(Config.powerNormalMonsterMin, Config.powerNormalMonsterMax);
		}

		trace ("valeurBase : " + valeurBase + " levelMonster : " + levelMonster + " Power : " + (valeurBase * (Math.pow(levelMonster, Config.powPowerMonsterbyLevel) * Config.coefPowerMonsterbyLevel)));
		//return valeurBase * levelMonster + (((Config.coefPowerMonsterbyLevel * level)* dungeonProgress()) / 100);
		// valeurBase+X^(Y)*Z
		return (valeurBase * (Math.pow(levelMonster, Config.powPowerMonsterbyLevel) * Config.coefPowerMonsterbyLevel)) ;
	}

	private static function findNameMonster():String
	{
		return "Michel";
	}

	private static function findImageMonster(strength:String, attr:String):Control
	{
		var bmpData:BitmapData = null;
		bmpData = Assets.getBitmapData('gfx/monster/$attr.png');
		var imageFromBmpData:Image = new Image(bmpData);
		var monControlImage:Control = new Control();
		
		if (strength == Monster.BOSS)
		{
			imageFromBmpData.tintMode = Image.TINTING_MULTIPLY;
			imageFromBmpData.tinting = 0.7;
			imageFromBmpData.color = 0xBF6BCB;
		}
		else if (strength == Monster.ELITE)
		{
			imageFromBmpData.tintMode = Image.TINTING_MULTIPLY;
			imageFromBmpData.tinting = 0.7;
			imageFromBmpData.color = 0xEAAB4D;
		}
		
		
		monControlImage.graphic = imageFromBmpData;

		return monControlImage;
	}
}