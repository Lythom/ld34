package ld34.scene;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Affix;
import ld34.crew.CrewMember;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.ButtonWithImg;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.FormatAlign;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.HXP;
import com.haxepunk.plateformer.BasicPhysicsEntity;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.display.BitmapData;
import haxe.EnumTools;
import ld34.ActionResolver;
import ld34.components.LevelGraph;
import ld34.crew.CrewMember;
import ld34.GameEvent.GameEventEnum;
import ld34.GameState;
import ld34.GeneratorWorld;
import ld34.Room;
import motion.Actuate;
import openfl.Assets;
import openfl.text.TextFormat;
import tools.attributes.Item;
import tools.image.Scale3XUpscaler;
import ld34.Config;
using Lambda;

/**
 * ...
 * @author Samuel Bouchet
 */

class LD34World extends Scene
{
	public static var instance:Scene ;

	private var bt_gauche: ButtonWithImg;
	private var bt_droite: ButtonWithImg;

	private var lb_gauche: Label;
	private var lb_droite: Label;

	private var p_joueur: Panel;

	private var p_map: Panel;
	private var lb_statjoueur: Label;
	var actionresolver:ActionResolver;
	var levelGraph:LevelGraph;


	private var p_menu: Panel;
	private var lb_menu: Label;
	private var bt_Menu:ButtonWithImg;

	private var aff_crew:Affix;
	
	
	private	var p_lore:Panel;
	private	var lb_lore:Label;
	
	private var count_crew:Int = 0;
	private var count_lvl:Int = 1;
	
	private var p_nivdonjon:Panel;
	private var lb_nivdonjon:Label;

	public function new()
	{
		super();

		instance = this;

		GameState.newGame();

		actionresolver = ActionResolver.getInstance();


		Label.addGlobalSyle("scissors", new TextFormat(null, null,0xEF6565 ));
		Label.addGlobalSyle("rock", new TextFormat(null, null,0x949494 ));
		Label.addGlobalSyle("paper", new TextFormat(null, null,0xE9D3A0 ));
	}

	override public function update()
	{
		super.update();
	}

	override public function begin()
	{
		super.begin();
		
		var bgbmpD:BitmapData = Assets.getBitmapData('gfx/bg.jpg');
		var bg:Control = new Control();
		bg.graphic = new Image(bgbmpD);
		add(bg);
		bg.layer = 999999;
		
		//var content:RoomContent = null;
		var content:Room = null;
		// bt gauche
		Control.useSkin('gfx/ui/transparent.png');
		bt_gauche = new ButtonWithImg("", 0, 0, 426, 700);

		//var bmpData:BitmapData = new BitmapData(346, 640, false, 0xE4CA6D);
		var bmpData:BitmapData  = Assets.getBitmapData("gfx/normalMapping/chapeau.png");
		var imageFromBmpData:Image = new Image(Scale3XUpscaler.upscale(bmpData));
		var monControlImage:Control = new Control();
		monControlImage.graphic = imageFromBmpData;
		bt_gauche.changeimage(monControlImage);
		bt_gauche.addEventListener(Button.CLICKED, function(e:ControlEvent)
		{
			// A gauche
			trace("A Gauche");
			actionresolver.resolveAction(GameState.room.roomL);

		});

		//content = GameState.room.roomL.contenu;
		content = GameState.room.roomL;
		if(content != null)
			MajBt(content,bt_gauche);


		// bt droite
		Control.useSkin('gfx/ui/transparent.png');
		bt_droite = new ButtonWithImg("Droite", 853, 0, 426, 700);
		var bmpData:BitmapData  = Assets.getBitmapData("gfx/normalMapping/cone.png");
		var imageFromBmpData:Image = new Image(Scale3XUpscaler.upscale(bmpData));
		var monControlImage:Control = new Control();
		monControlImage.graphic = imageFromBmpData;
		bt_droite.changeimage(monControlImage);
		bt_droite.addEventListener(Button.CLICKED, function(e:ControlEvent)
		{
			trace("A Droite");
			actionresolver.resolveAction(GameState.room.roomR);

		});
		//content =  GameState.room.roomR.contenu;
		content =  GameState.room.roomR;
		if (content != null)
			MajBt(content,bt_droite);

		//lb_gauche = new Label("Salle Gauche", 0, 701, 426, 100);
		//lb_droite = new Label("Salle Droite", 853, 701, 426, 100);

		// HUD : Crew
		aff_crew = new Affix(1278);
		aff_crew.localX = 8;
		aff_crew.localY = 8;
		Control.useSkin('gfx/ui/blueMagda.png');
		p_joueur = new Panel( 2, 701, 1278, 100, true);
		p_joueur.addControl(aff_crew);
		count_crew = 0;
		var crewMembers:List<CrewMember> = GameState.armee.items.map(function itemToCrewMember(item:Item):CrewMember { return cast(item, CrewMember); } );
		for (cm in crewMembers) {
			aff_crew.addControl(cm.imageIcon);
			count_crew++;
		}

		// Donjon

		var statjoueur:String=null;
		statjoueur=MajStat();
		lb_statjoueur = new Label("Vous êtes ici:", 0, 650, 426, 40, FormatAlign.CENTER, VerticalAlign.CENTER);
		lb_statjoueur.useXmlFormat = true;
		lb_statjoueur.size = 18;
		Control.useSkin('gfx/ui/transparent.png');
		p_map =  new Panel(426, 0, 426, 700, false);
		p_map.addControl(lb_statjoueur);

		// Message crew
		Control.useSkin('gfx/ui/blueMagda.png');
		lb_lore = new Label("BLABLABLA", 10, 0, 0, 0, FormatAlign.CENTER, VerticalAlign.CENTER);
		p_lore = new Panel(20, 700, 1, 40, true);		
		p_lore.addControl(lb_lore);
		
		
		levelGraph = new LevelGraph(0, 0, 426, 650);
		levelGraph.display(GameState.room, Config.deepArmyView);
		p_map.addControl(levelGraph);

		// Panel menu
		Control.useSkin('gfx/ui/blueMagda.png');
		p_menu = new Panel(425, 189, 426, 250, true);
		p_menu.hide();
		lb_menu = new Label("", 0, 40, 426, 0,FormatAlign.CENTER);
		lb_menu.size = 26;
		bt_Menu = new ButtonWithImg("Back to Menu", 40, 150, 346, 60);
		bt_Menu.addEventListener(Button.CLICKED, function(e:ControlEvent)
		{
			HXP.scene = new WelcomeWorld();

		});
		p_menu.addControl(lb_menu);
		p_menu.addControl(bt_Menu);
		
		// avancement donjon
		Control.useSkin('gfx/ui/blueMagda.png');
		p_nivdonjon = new Panel(763, 0, 85, 45);
		lb_nivdonjon = new Label("", 15, 0, 0, 40, FormatAlign.CENTER, VerticalAlign.CENTER);
		lb_nivdonjon.color = 0x8C0404;
		lb_nivdonjon.text = Std.string(count_lvl) + "/" + Std.string(Config.numberOfLevel); 
		p_nivdonjon.addControl(lb_nivdonjon);
		
		// Construction scene
		add(bt_gauche);
		add(bt_droite);
		add(p_joueur);
		add(p_map);
		add(p_menu);
		p_menu.layer = 0;
		add(lb_lore);
		lb_lore.color = 0x000000;
		lb_lore.layer = 0;
		add(p_lore);
		p_lore.layer = 0;
		p_lore.hide();

		add(p_nivdonjon);
		p_nivdonjon.layer = 0;

		actionresolver.addEventListener(GameEvent.GAME_EVENT_HAPPENED, handleEvent);
		
		bt_gauche.addEventListener(Button.MOUSE_HOVER, function(e) {
			levelGraph.previewLeft();
			aff_crew.placeChilds();
			animateCrew(null);
			animateCrew(GameState.room.roomL);
		});
		bt_gauche.addEventListener(Button.MOUSE_OUT, function(e) {
			levelGraph.resetPreview();
			animateCrew(null);
		});
		bt_droite.addEventListener(Button.MOUSE_HOVER, function(e) {
			levelGraph.previewRight();
			aff_crew.placeChilds();
			animateCrew(null);
			animateCrew(GameState.room.roomR);
		});
		bt_droite.addEventListener(Button.MOUSE_OUT, function(e) {
			levelGraph.resetPreview();
			animateCrew(null);
		});

		
		afflore();


		var statjoueur=MajStat();
		lb_statjoueur.text = statjoueur;
	}
	
	function animateCrew(room:Room = null) 
	{
		var crewMembers:List<CrewMember> = GameState.armee.items.map(function itemToCrewMember(item:Item):CrewMember { return cast(item, CrewMember); } );
		if (room == null || room.contenu == null) {
			for (c in crewMembers) {
				c.animationStop();
			}
		} else {
			switch (room.contenu) 
			{
				case RoomContent.MonsterFight(monster):
					for (c in crewMembers) {
						if (c.type == GameState.strongAgainst(monster.attr)) {
							c.animateAsAttacker();
						}
						if (c.type == GameState.weakAgainst(monster.attr)) {
							c.animateAsDefender();
						}
					}
					for (c in crewMembers) {
						c.animateWillLevelUp(monster.level);
					}
					
				case RoomContent.CrewMember(crewMember):
			}
		}
	}

	override public function end()
	{
		super.end();
		actionresolver.removeEventListener(GameEvent.GAME_EVENT_HAPPENED,handleEvent);

	}


	public function MajBt(content:Room, bt:ButtonWithImg):Void
	{

		var str_typeroom:String;

		switch (content.contenu)
		{
			case RoomContent.MonsterFight(monster):
			{
				str_typeroom = Std.string(monster.name);
				bt.label.text = str_typeroom;
				bt.changeimage(monster.image);
				bt.lb_stat.text = "FIGHT monster lvl." + Std.string(monster.level) + "\nType : " + Std.string(monster.attr) + " - Power : " + Std.string(monster.value);
				bt.lb_stat.color = 0x96290A;

			}

			case RoomContent.CrewMember(crewMember):
			{
				str_typeroom = Std.string(crewMember.name);
				bt.label.text = str_typeroom;
				bt.changeimage(crewMember.image);
				bt.lb_stat.text = "RECRUIT ally lvl." + Std.string(crewMember.level) + "\nType : " + Std.string(crewMember.type) + " - Power : " + Std.string(crewMember.value);
				bt.lb_stat.color = 0x1B307A;
			}
		}
	}

	public function MajStat()
	{
		var statjoueur:String = "";

		//statjoueur = "<red>Scisor: "+ GameState.armee.get(GameState.CISOR).value+"</red> - <grey>Rock:"+GameState.armee.get(GameState.ROCK).value+"</grey> - <green>Paper:"+GameState.armee.get(GameState.PAPER).value+"</green>";
		statjoueur = "<scissors>Scisor "+GameState.armee.get(GameState.CISOR).value+ "</scissors> - <rock>Rock "+GameState.armee.get(GameState.ROCK).value+"</rock> - <paper>Paper "+GameState.armee.get(GameState.PAPER).value+"</paper>";
		return statjoueur;
	}

	function handleEvent(gameevent:GameEvent)
	{
		var statjoueur=MajStat();
		lb_statjoueur.text = statjoueur;
		
		var nextRoom:Room = gameevent.nextRoom;

		switch(gameevent.gameEvent)
		{
			case GameEventEnum.actionStart(roomcontent):
				trace("Start !!! combat anim");

			case GameEventEnum.actionResolved(actionresult):
				trace("Resolved !!! result anim");
				switch (actionresult)
				{
					case ActionResult.MonsterSuccess(deadCrew, monster):

						//lb_statjoueur.text = "Victory ! Are dead : " + deadCrew.map(function(c:CrewMember) { return c.toString() + ','; });
						for (member in deadCrew)
						{
							member.die();
							count_crew--;
						}
						cast(monster.image.graphic, Image).angle = 0;
						Actuate.tween(cast(monster.image.graphic, Image), 1, {
							angle: -720,
							scale: 0
						});
						
						
					case ActionResult.GameOver(deadCrew):
						// TODO : visuall representation of the action
						//lb_statjoueur.text = "Game Over";
						var type:String = " of one type.";
						for (member in deadCrew)
						{
							type = member.type;
							member.die();
							count_crew--;
						}
						lb_menu.text = 'Game Over\nYou have no more $type';
						lb_menu.color = 0x303236;
						p_menu.show();


					case ActionResult.Recruitment(recruited):
						//lb_statjoueur.text = "recruited : " + recruited.map(function(c:CrewMember) { return c.toString() + ','; });
						// TODO : visuall representation of the action
						for (member in recruited)
						{
							if (GameState.room.roomR == nextRoom){
								bt_droite.deleteimage();
								moveCrew(member, 900);
							} else {
								bt_gauche.deleteimage();
								moveCrew(member, 200);
							}
						}	

					case ActionResult.GameWon:
						//lb_statjoueur.text = "Gagné !";
						lb_menu.text = "Victory !";
						p_menu.show();
				}

			case GameEventEnum.gotoNextRoomTransition(room):
				trace("Moving…");
				GeneratorWorld.GenerateContenuRoom(GameState.room);
				var offset:Int = 71;
				if (GameState.room.roomR == room) {
					offset = -71;
					levelGraph.resetPreview();
					levelGraph.previewRight();
				} else {
					levelGraph.resetPreview();
					levelGraph.previewLeft();
				}
				Actuate.tween(levelGraph, 1, 
					{ 
						localX : levelGraph.localX + offset, 
						localY: levelGraph.localY + 130 
					}).onComplete(function() {
						levelGraph.localX = levelGraph.localY = 0;
						levelGraph.resetPreview();
					});

			case GameEventEnum.arrivedToNewRoom(room):
				trace("GOTO room " + room);
				MajBt(GameState.room.roomL,bt_gauche);
				MajBt(GameState.room.roomR, bt_droite);
				levelGraph.display(GameState.room, Config.deepArmyView);
				count_lvl++;
				lb_nivdonjon.text = Std.string(count_lvl) + "/" + Std.string(Config.numberOfLevel); 
				aff_crew.placeChilds();

		}
	}
	
	function moveCrew(member:CrewMember, startingX:Int) 
	{
		//var monControlImage:Control = new Control();
		//monControlImage.graphic = member.imageIcon;
		add(member.imageIcon);
		member.imageIcon.layer = 0;
		var posx:Float = 3+48*count_crew+8;
		var posy:Float = 701+8;
		member.imageIcon.localX = startingX;
		member.imageIcon.localY = 250;
		Actuate.tween(member.imageIcon, 2, 
		{ 
			localX: posx, 
			localY: posy
		}).onComplete(function() {
			
			aff_crew.addControl(member.imageIcon);
			count_crew++;
		});
	}
	
	public function afflore()
	{
		var x:Int;
		var lore:String;
		var crewMembers:List<CrewMember> = GameState.armee.items.map(function itemToCrewMember(item:Item):CrewMember { return cast(item, CrewMember); } );
		
		var member:CrewMember = Random.fromIterable(crewMembers);
		
		
		Actuate.timer(5).onComplete(function() {
			lore = Std.string(member.lore);
			lb_lore.text = lore;
			p_lore.width =  lb_lore.width+20;
			x = Random.int(2, 1280);
			if (x + p_lore.width >= 1280)
				x = 1280 - p_lore.width;
			p_lore.x = x;
			p_lore.y = Random.int(700, 760);
				
			p_lore.show();
			
			Actuate.timer(5).onComplete(function() {
				p_lore.hide();
				afflore();
				
			});	
			
		});			
	}

}