package ld34.scene;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import ld34.scene.LD34World;
import openfl.Assets;
import openfl.display.BitmapData;


/**
 * ...
 * @author Samuel Bouchet
 */

class WelcomeWorld extends Scene
{
	private var title:Label;
	private var play2:Button;
	var title2:Label;
	var title3:Label;
	var play:Button;
	var play3:Button;
	
	public static var instance:Scene ;

	public function new()
	{
		super();
		
		var bgbmpD:BitmapData = Assets.getBitmapData('gfx/bg.jpg');
		var bg:Control = new Control();
		bg.graphic = new Image(bgbmpD);
		add(bg);
		bg.layer = 999999;
		
		
		title = new Label("Enlarge your Shifumi", 210, 92);
		title.size = 150;
		title.color = 0xE9D3A0;
		title.font = Assets.getFont('font/rps.ttf').fontName;
		
		title2 = new Label("Enlarge your Shifumi", 212, 94);
		title2.size = 150;
		title2.color = 0xEF6565;
		title2.font = Assets.getFont('font/rps.ttf').fontName;
		
		title3 = new Label("Enlarge your Shifumi", 214, 96);
		title3.size = 150;
		title3.color = 0x949494;
		title3.font = Assets.getFont('font/rps.ttf').fontName;

		GameState.guiUseSkin(Random.fromArray([GameState.PAPER,GameState.CISOR,GameState.ROCK]));
		play = new Button("Play", 250, 371, 200,150);
		play.size = 60;
		play.addEventListener(Button.CLICKED, function(e:ControlEvent) {
			HXP.scene = new LD34World();	
			
		});
		
		
		GameState.guiUseSkin(Random.fromArray([GameState.PAPER,GameState.CISOR,GameState.ROCK]));
		play2 = new Button("Play", 550, 371, 200,150);
		play2.size = 60;
		play2.addEventListener(Button.CLICKED, function(e:ControlEvent) {
			HXP.scene = new LD34World();	
			
		});
		
		GameState.guiUseSkin(Random.fromArray([GameState.PAPER,GameState.CISOR,GameState.ROCK]));
		GameState.guiUseSkin(GameState.CISOR);
		play3 = new Button("Play", 850, 371, 200,150);
		play3.size = 60;
		play3.addEventListener(Button.CLICKED, function(e:ControlEvent) {
			HXP.scene = new LD34World();	
			
		});
		

		instance = this;
	}
	
	override public function update()
	{
		super.update();
	}
	
	override public function begin()
	{
		add(title3);
		add(title2);
		add(title);

		add(play);
		//add(play2);
		add(play3);
		
		super.begin();
	}
	
	override public function end()
	{
		removeAll();
		super.end();
	}
	
}