package com.haxepunk.gui;

import com.haxepunk.Graphic;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.FormatAlign;
import com.haxepunk.gui.graphic.NineSlice;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.text.TextFormatAlign;

/**
 * Ui control: Affix.
 */
class Affix extends Control
{

	static public inline var ADDED_TO_CONTAINER:String = "added_to_container";
	static public inline var REMOVED_FROM_CONTAINER:String = "removed_from_container";
	static public inline var ADDED_TO_WORLD:String = "added_to_world";
	static public inline var REMOVED_FROM_WORLD:String = "removed_from_world";
	static public inline var HIDDEN:String = "hidden";
	static public inline var SHOWN:String = "shown";
	static public inline var MOUSE_HOVER:String = "mouseHover";
	static public inline var MOUSE_OUT:String = "mouseOut";
	static public inline var RESIZED:String = "resized";

	/**
	 * Use this to fit your button skin's borders, set the default padding of every new Button and ToggleButton.
	 * padding attribute can be changed on instances after creation.
	 */
	public static var defaultPadding:Int = 0;


	/**
	 * Create a Button with another control inside.
	 * @param x					X coordinate of the button.
	 * @param y					Y coordinate of the button.
	 * @param text				Text of the button
	 * @param width				Width of the button's hitbox.
	 * @param height			Height of the button's hitbox.
	 */
	public function new(maxWidth:Int = 0)
	{
		_overCalled = false;
		padding = Affix.defaultPadding;
		_enabled = true;

		_autoHeight = true;
		_autoWidth = true;
		_maxWidth = maxWidth;

		super(x, y, width, height);
		setHitbox(width, height);

		// visual update
		updateSize();
		update();
	}

	/**
	 * Update the button to be displyed properly after it has been resized.
	 * Automatically called.
	 */
	public function updateSize(?e:Event):Void
	{

		// update button size according to the content
		if (autoWidth) {
			width = calculateWidth();
		}
		if (autoHeight) {
			height = calculateHeight();
		}

		dispatchEvent(new ControlEvent(this, RESIZED));
	}
	
	function calculateHeight() 
	{
		var h:Int = 0;
		for (c in this.children) {
			h = Math.ceil(Math.max(c.height, h));
		}
		return h + padding;
	}
	
	function calculateWidth() 
	{
		var w:Int = 0;
		for (c in this.children) {
			w = Math.ceil(Math.max(c.localX + c.width, w));
		}
		return w + padding*2;
	}
	
	override public function addControl(child:Control, ?position:Int):Void 
	{
		super.addControl(child, position);
		placeChilds();
		updateSize();
	}
	
	override public function removeControl(child:Control):Bool 
	{
		var removed:Bool = super.removeControl(child);
		placeChilds();
		updateSize();
		return removed;
	}
	
	
	override public function removeAllControls():Void 
	{
		super.removeAllControls();
		placeChilds();
		updateSize();
	}
	public function placeChilds():Void 
	{
		for (i in 0...this.children.length) {
			var child:Control = this.children[i];
			if (i > 0) {
				var lastChild:Control = this.children[i - 1];
				if (lastChild != null) {
					child.localX = lastChild.localX + lastChild.width + padding;
					child.localY = lastChild.localY;
				}
			} else {
				child.localX = padding;
				child.localY = padding;
			}
			
			if (child.localX + child.width > _maxWidth) {
				child.localX = padding;
				child.localY += child.height;
			}
		}
	}

	/**
	 * Amount to pad between button edge and label
	 */
	public var padding(get_padding, set_padding):Int;
	private function get_padding():Int { return _padding; }
	private function set_padding(value:Int):Int {
		_padding = value;
		updateSize();
		return _padding;
	}

	// change graphic when turning to disabled
	override private function set_enabled(value:Bool):Bool
	{
		return super.set_enabled(value);
	}
	function get_autoHeight():Bool 
	{
		return _autoHeight;
	}
	function set_autoHeight(value:Bool):Bool 
	{
		return _autoHeight = value;
	}
	public var autoHeight(get_autoHeight, set_autoHeight):Bool;
	
	function get_autoWidth():Bool 
	{
		return _autoWidth;
	}
	function set_autoWidth(value:Bool):Bool 
	{
		return _autoWidth = value;
	}
	public var autoWidth(get_autoWidth, set_autoWidth):Bool;

	override public function added()
	{
		super.added();
	}

	override public function removed()
	{
		super.removed();
	}

	override public function toString():String
	{
		return this._class;
	}

	private var _padding:Int;
	private var _align:FormatAlign;
	private var _autoWidth:Bool;
	private var _autoHeight:Bool;
	private var _maxWidth:Int;
}