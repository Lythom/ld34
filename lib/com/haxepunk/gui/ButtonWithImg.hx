package com.haxepunk.gui;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import openfl.Assets;

/**
 * ...
 * @author Red
 */
class ButtonWithImg extends Button
{

	private var imgdejacharge:Control;
	public var lb_stat:Label;
	public var p_stat:Panel;
	
	public function new(text:String="Button", x:Float=0, y:Float=0, width:Int=0, height:Int=0) 
	{
		super(text, x, y, width, height);
		imgdejacharge = null;
		//new Control(this.x, this.y, this.width, this.height);
		Control.useSkin('gfx/ui/blueMagda.png');
		p_stat = new Panel(0, 600, this.width, 100, true);
		lb_stat = new Label("Stat", 10, 0, this.width, 100, FormatAlign.LEFT, VerticalAlign.CENTER);
		lb_stat.size = 20;
		p_stat.addControl(lb_stat);
		this.addControl(p_stat);
	}
	public function changeimage(image:Control)
	{				
		if (imgdejacharge != null) {
			this.removeControl(imgdejacharge);
		}
		
		image.set_localX(10);
		image.set_localY(100);
		this.addControl(image);
		imgdejacharge = image;	
	}

	
	public function deleteimage()
	{
		this.removeControl(imgdejacharge);
	}
	
}