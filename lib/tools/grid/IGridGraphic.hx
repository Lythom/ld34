package tools.grid;
import com.haxepunk.Graphic;
import flash.geom.Point;

/**
 * ...
 * @author Lythom
 */

interface IGridGraphic
{
	function getCellPosition(cell:ICell):Point;
	function getCellAt(p:Point):ICell;
	function getHeight():Int;
	function getWidth():Int;
	var cellWidth(get_cellWidth, null):Int;
	var cellHeight(get_cellHeight, null):Int;
}