package tools.grid;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Image;
import com.haxepunk.HXP;
import com.haxepunk.utils.Draw;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;
	
/**
 * ...
 * @author Samuel Bouchet
 */
class AxonometricGridGraphic extends Image implements IGridGraphic
{
	private var _grid:Grid;
	private var _displayWidth:Int;
	private var _displayHeight:Int;
	private var _perspectiveOffset:Int;
	private var _cellWidth:Int;
	private var _cellHeight:Int;
	private var _position:Point;
	
	/**
	 * 
	 * @param	grid
	 * @param	displayWidth
	 * @param	displayHeight
	 * @param	perspectiveOffset	Half width to have at the bottom of the grid in addition to base (top) width. BottomWidth = displayWidth + offsetWidth*2;
	 */
	public function new(grid:Grid, displayWidth:Int, displayHeight:Int, perspectiveOffset:Int)
	{
		super(new BitmapData(displayWidth, displayHeight, true, 0x00000000), new Rectangle(0, 0, displayWidth + 1 + perspectiveOffset*2, displayHeight + 1));
		this.perspectiveOffset = perspectiveOffset;
		this.displayHeight = displayHeight;
		this.displayWidth = displayWidth;
		this.grid = grid;
		this.cellWidth = Math.round(displayWidth / grid.columnCount);
		this.cellHeight = Math.round(displayHeight / grid.lineCount);

		draw();
	}
	
	public function draw() {
		
		Draw.setTarget(_buffer);
		
		var pourc:Float;
		// trace toutes les lignes verticales
		var iWidth:Int = 0 ;
		while (iWidth <= displayWidth) {
			pourc = iWidth / displayWidth;
			Draw.linePlus(iWidth+perspectiveOffset, 0, Math.round(iWidth - perspectiveOffset + perspectiveOffset * 2 * pourc +perspectiveOffset), displayHeight, 0xFFFFFF, 0.25, 1);
			iWidth += cellWidth;
		}
		
		// trace toutes les lignes horizontales
		var iHeight:Int = 0 ;
		while (iHeight <= displayHeight) {
			pourc = iHeight / displayHeight;
			Draw.linePlus( Math.round(- perspectiveOffset * pourc) + perspectiveOffset, iHeight, Math.round(displayWidth + perspectiveOffset * pourc)+perspectiveOffset, iHeight, 0xFFFFFF, 0.25, 1);
			iHeight += cellHeight;
		}
	}
	
	public function getCellPosition(cell:ICell):Point {
		var pourc:Float = cell.x / grid.columnCount;
		var amplitude:Float = ((cell.y+1) / grid.lineCount) * perspectiveOffset; // +1 to align on bottom left point of the cell
		
		var position = new Point();
		position.x = (cell.x) * cellWidth 				// cell position
					- amplitude + amplitude * 2 * pourc // perspective offset deformation
					+ (amplitude / grid.columnCount)	// bottom center
					+ perspectiveOffset;				// perspective offset global image offset
		position.y = (cell.y) * cellHeight;
		return position;
	}
	
	public function getCellAt(p:Point):ICell {
		
		if(grid!=null){
			for (x in 0...grid.columnCount) {
				for (y in 0...grid.lineCount) {
					var cell:Cell = grid.cells[x][y];
					var pourc:Float = cell.x / grid.columnCount;
					var amplitude:Float = (cell.y / grid.lineCount) * perspectiveOffset;
					var rect = new Rectangle();
					rect.x = grid.x + (cell.x) * cellWidth 		// cell position
							- amplitude + amplitude * 2 * pourc // perspective offset deformation
							+ perspectiveOffset;				// perspective offset global image offset
					rect.y = grid.y + (cell.y) * cellHeight;
					rect.width = displayWidth / grid.columnCount;
					rect.height = displayHeight / grid.lineCount;
					if (rect.containsPoint(p)) {
						return cell;
					}
				}
			}
		}
		return null;
	}
	
	public function getWidth():Int
	{
		return Math.floor(width * HXP.screen.scale );
	}
	public function getHeight():Int
	{
		return Math.floor(height * HXP.screen.scale);
	}
	
	private function get_displayWidth():Int
	{
		return _displayWidth;
	}
	
	private function set_displayWidth(value:Int):Int
	{
		return _displayWidth = value;
	}
	
	public var displayWidth(get_displayWidth, set_displayWidth):Int;
	
	private function get_displayHeight():Int
	{
		return _displayHeight;
	}
	
	private function set_displayHeight(value:Int):Int
	{
		return _displayHeight = value;
	}
	
	public var displayHeight(get_displayHeight, set_displayHeight):Int;
	
	private function get_perspectiveOffset():Int
	{
		return _perspectiveOffset;
	}
	
	private function set_perspectiveOffset(value:Int):Int
	{
		return _perspectiveOffset = value;
	}
	
	public var perspectiveOffset(get_perspectiveOffset, set_perspectiveOffset):Int;
	
	private function get_grid():Grid
	{
		return _grid;
	}
	
	private function set_grid(value:Grid):Grid
	{
		return _grid = value;
	}
	
	public var grid(get_grid, set_grid):Grid;
	
	function get_cellWidth():Int 
	{
		return _cellWidth;
	}
	function set_cellWidth(value:Int):Int 
	{
		return _cellWidth = value;
	}
	public var cellWidth(get_cellWidth, set_cellWidth):Int;
	
	function get_cellHeight():Int 
	{
		return _cellHeight;
	}
	function set_cellHeight(value:Int):Int 
	{
		return _cellHeight = value;
	}
	public var cellHeight(get_cellHeight, set_cellHeight):Int;

	
}