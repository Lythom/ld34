package tools.grid;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.gui.Control;
import com.haxepunk.HXP;
import flash.geom.Point;
import haxe.ds.HashMap.HashMap;
import tools.grid.Cell;
import tools.grid.GridObject;

/**
 * ...
 * @author Samuel Bouchet
 */
class HexGrid extends Control
{
	private var _cells:Map<String,ICell>;
	private var _lineCount:Int;
	private var _columnCount:Int;
	private var _toRemove:Array<Control>;
	private var _toAdd:Array<GridObject>;
	private var _gridObjectToRemove:Bool;
	
	
	/**
	 *
	 * @param	x			x position of the entity
	 * @param	y			y position of the entity
	 * @param	gridRadius	size of the hex grid from the center. 1 would be only one cell, 2 is 7 cells, etc.
	 */
	public function new(x:Int, y:Int, gridRadius:Int)
	{
		super(x, y);
		
		this.lineCount = lineCount;
		this.columnCount = columnCount;
		_cells = new Map<String,ICell>;
		
		// populate cell array with new cells
		var pos:Point = new Point(0, 0);
		var c:Cell;
		for (r in 0...gridRadius) {
			_cells[x] = new Array<Cell>();
			for (y in 0...lineCount) {
				c =  new Cell(this, x, y);
				_cells[x][y] = c;
			}
		}
		
		_toRemove = new Array<Control>();
		_toAdd = new Array<GridObject>();
		_gridObjectToRemove = false;
		
	}
	
	/**
	 * remove every object from the cells
	 */
	public function clearCellObjects() {
		for (x in 0...columnCount) {
			for (y in 0...lineCount) {
				if (getCell(x, y).gridObject != null) {
					getCell(x, y).gridObject = null;
				}
			}
		}
	}
	
	/**
	 * Return a cell if any exists at x,y.
	 * Return null if cell not found.
	 */
	public function getCell(x:Int, y:Int):Cell {
		if (x >= 0 && x < _cells.length) {
			if (y >= 0 && y < _cells[x].length) {
				return _cells[x][y];
			}
		}
		return null;
	}
	
	override public function update() 
	{
		//remove entities that are not in a cell
		if (_gridObjectToRemove) {
			removeUnattachedGridObjects();
		}
		// add object that should be added
		addGridObjects();
		
		super.update();
	}
	
	/**
	 * Do remove grid objects on the "toRemove" list
	 */
	private function removeUnattachedGridObjects() 
	{
		for (c in children) {
			var go:GridObject;
			if (Std.is(c, GridObject)) {
				go = cast(c, GridObject);
				if (go.cell == null) {
					_toRemove.push(c);
				}
			}
		}
		for (c in _toRemove) {
			super.removeControl(c);
		}
		_toRemove.splice(0, _toRemove.length);
		_gridObjectToRemove = false;
	}
	
	/**
	 * Do add grid objects on the "toAdd" list
	 */
	private function addGridObjects() {
		var control:GridObject ;
		while ((control = _toAdd.pop()) != null) {
			if (control.cell != null) {
				// place X/Y
				var igg:IGridGraphic = cast(graphic, IGridGraphic);
				var pos:Point = igg.getCellPosition(control.cell);
				control.localX = pos.x;
				control.localY = pos.y;
				// one layer per line
				if (!contains(control)) {
					super.addControl(control);
				}
				control.layer = layer - control.cell.y;
			}
		}
	}
	
	/**
	 * Specific way to add GridObject (at next update) to prevent pb with world add/remove
	 * @param	child
	 */
	override public function addControl(child:Control, ?position:Int):Void 
	{
		if (Std.is(child, GridObject)) {
			_toAdd.push(cast(child, GridObject));
		} else {
			super.addControl(child, position);
		}
		
	}
	
	/**
	 * Specific way to remove GridObject (at next update) to prevent pb with world add/remove
	 * @param	child
	 */
	override public function removeControl(child:Control):Void 
	{
		if (Std.is(child, GridObject)) {
			var go:GridObject = cast(child, GridObject);
			go.cell = null;
			_gridObjectToRemove = true;
		} else {
			super.removeControl(child);
		}
		
	}
	
	/* INTERFACE grid.IGridGraphic */
	public function getCellPosition(cell:Cell):Point
	{
		return cast(graphic, IGridGraphic).getCellPosition(cell);
	}
	
	public function getCellAt(p:Point):Cell
	{
		return cast(graphic, IGridGraphic).getCellAt(p);
	}
	
	public var cellWidth(get_cellWidth, null):Int;
	function get_cellWidth() 
	{
		return cast(graphic, IGridGraphic).cellWidth;
	}
	public var cellHeight(get_cellHeight, null):Int;
	
	function get_cellHeight() 
	{
		return cast(graphic, IGridGraphic).cellHeight;
	}
	
	override private function set_graphic(value:Graphic):Graphic
	{
		if (!Std.is(value, IGridGraphic)) {
			throw "graphic must be an IGridGraphic";
		}
		var gridGraphic:IGridGraphic = cast(value, IGridGraphic);
		width = gridGraphic.getWidth(); // Math.floor(cast(value, AxonometricGridGraphic).width * HXP.screen.scale);
		height = gridGraphic.getHeight();  // Math.floor(cast(value, AxonometricGridGraphic).height * HXP.screen.scale);
		
		return super.set_graphic(value);
	}

	/* GETTERS AND SETTERS */
	
	private function get_cells():Array<Array<Cell>>
	{
		return _cells;
	}
	
	private function set_cells(value:Array<Array<Cell>>):Array<Array<Cell>>
	{
		return _cells = value;
	}
	
	public var cells(get_cells, set_cells):Array<Array<Cell>>;
	
	private function get_lineCount():Int
	{
		return _lineCount;
	}
	
	private function set_lineCount(value:Int):Int
	{
		return _lineCount = value;
	}
	
	public var lineCount(get_lineCount, set_lineCount):Int;
	
	private function get_columnCount():Int
	{
		return _columnCount;
	}
	
	private function set_columnCount(value:Int):Int
	{
		return _columnCount = value;
	}
	
	public var columnCount(get_columnCount, set_columnCount):Int;
}