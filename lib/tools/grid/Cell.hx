package tools.grid;
import com.haxepunk.gui.Control;
import flash.geom.Point;

/**
 * ...
 * @author Samuel Bouchet
 */
class Cell implements ICell
{
	private var _grid:Grid;
	private var _x:Int;
	private var _y:Int;
	private var _gridObject:GridObject;
	
	public function new(grid:Grid, x:Int, y:Int)
	{
		this.y = y;
		this.x = x;
		this.grid = grid;
		
	}
	
	public function upperCell():ICell {
		return this.grid.getCell(x, y - 1);
	}
	public function bottomCell():ICell {
		return this.grid.getCell(x, y + 1);
	}
	public function leftCell():ICell {
		return this.grid.getCell(x-1, y);
	}
	public function rightCell():ICell {
		return this.grid.getCell(x+1, y);
	}
	
	public function upperRightCell():ICell {
		return this.grid.getCell(x+1, y - 1);
	}
	public function upperLeftCell():ICell {
		return this.grid.getCell(x-1, y - 1);
	}
	public function bottomLeftCell():ICell {
		return this.grid.getCell(x-1, y+1);
	}
	public function bottomRightCell():ICell {
		return this.grid.getCell(x+1, y+1);
	}
	
	public function adjacentCells():Array<ICell> {
		var a:Array<ICell> = new Array<ICell>();
		if (upperCell() != null) {
			a.push(upperCell());
		}
		if (rightCell() != null) {
			a.push(rightCell());
		}
		if (bottomCell() != null) {
			a.push(bottomCell());
		}
		if (leftCell() != null) {
			a.push(leftCell());
		}
		return a;
	}
	
	public function neighbourCells():Array<ICell> {
		var a:Array<ICell> = adjacentCells();
		if (upperLeftCell() != null) {
			a.push(upperLeftCell());
		}
		if (upperRightCell() != null) {
			a.push(upperRightCell());
		}
		if (bottomLeftCell() != null) {
			a.push(bottomLeftCell());
		}
		if (bottomRightCell() != null) {
			a.push(bottomRightCell());
		}
		
		return a;
	}
	
	private function get_grid():Grid
	{
		return _grid;
	}
	
	private function set_grid(value:Grid):Grid
	{
		return _grid = value;
	}
	
	public var grid(get_grid, set_grid):Grid;
	
	private function get_x():Int
	{
		return _x;
	}
	
	private function set_x(value:Int):Int
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Int;
	
	private function get_y():Int
	{
		return _y;
	}
	
	private function set_y(value:Int):Int
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Int;
	
	private function set_gridObject(value:GridObject):GridObject
	{
		return _gridObject = value;
	}
	
	private function get_gridObject():GridObject
	{
		return _gridObject;
	}
	
	public var gridObject(get_gridObject, set_gridObject):GridObject;
}