package tools.grid;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Tooltip;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import tools.grid.Cell;
import tools.grid.GridObject;
import flash.geom.Point;

/**
 * ...
 * @author Samuel Bouchet
 */
class Utopiales2012Grid extends Grid
{

	private var gridGraphic:AxonometricGridGraphic;
	private var tt:Tooltip ;
	private var lastC:Cell;
	
	/**
	 *
	 * @param	rowCount				number of cells horizontally
	 * @param	lineCount				number of cells verticaly
	 * @param	perspectiveOffset	perspective effect difference between top and bottom line in px
	 */
	public function new(x:Int, y:Int, rowCount:Int, lineCount:Int)
	{
		super(x, y, rowCount, lineCount);
		tt = new Tooltip();
	}
	
	override private function setGraphic(value:Graphic):Graphic
	{
		gridGraphic = cast(value, AxonometricGridGraphic);
		return super.setGraphic(value);
	}
	
	override public function update():Dynamic
	{
		super.update();
		
		if (Input.mouseX > x && Input.mouseY > y
			&& Input.mouseX < x + gridGraphic.width && Input.mouseY < y + gridGraphic.height){
		
			var c:Cell = getCell(new Point(Input.mouseX, Input.mouseY));
			if (lastC != c) {
				tt.hide();
				if (c != null) {
					if (c.gridObject != null) {
						tt.showTip(c.gridObject.getToolTip() , c.gridObject);
					}
				}
				lastC = c;
			}
			
		} else if(tt.visible){
			tt.hide();
		}
	}
	
}